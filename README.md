# 前端必备技能 webpack 

 
> 作者: 她不美却常驻我心
> 
> 博客地址: [https://blog.csdn.net/qq_39506551](https://blog.csdn.net/qq_39506551)
> 
> 微信公众号：老王的前端分享
> 
> 每篇文章纯属个人经验观点，如有错误疏漏欢迎指正。转载请附带作者信息及出处。


&emsp;
***

&emsp;&emsp;本仓库为博客内容同步代码，搭配博客食用更佳，博客地址：[前端必备技能 webpack - 教程目录](https://blog.csdn.net/qq_39506551/article/details/109068959)。

&emsp;&emsp;相关内容请以博客为准，本仓库仅为相关内容代码。

***
&emsp;

## 采用版本：

-  `node` 版本 `13.9.0` 
- `webpack` 版本 `4.44.2` 和 `5.1.0`
- `webpack-cli` 版本 `3.3.12` 和 `4.0.0`

就像我们先学习 `JS` 基本语法，然后在学习 `ES6` 等新特性一样，我们先学习 `webpack4` 之后，在学习 `webpack5`。

***

## 使用说明：

&emsp;&emsp;① 借助 `NodeJs` 会在当前目录查找 `node_modules` ，如果没有找到会向上级目录查询的特性，所有的安装操作都是在 `webpack4-demo` 和 `webpack5-demo` 中进行的：

&emsp;&emsp;&emsp;&emsp;`webpack4-demo` 中安装了 `webpack@4.44.2` 和 `webpack-cli@3.3.12`


&emsp;&emsp;&emsp;&emsp;`webpack5-demo` 中安装了 `webpack@5.1.0` 和 `webpack-cli@4.0.0`


&emsp;&emsp;② 每一个示例中具有一个 `webpack.config.js` ，该文件为该示例的配置文件，在当前示例的路径下运行 `webpack` 指令即可，如未产生预期的情况，请尝试打包时指定相应配置文件在运行，或通过公众号等方式与作者联系。


&emsp;&emsp;③ 每一个对应的示例在博客中均有相应说明，这里不再加以赘述。


***

&emsp;

> 每篇文章纯属个人经验观点，如有错误疏漏欢迎指正。
>
> 转载请附带作者信息及出处。您的评论和关注是我更新的动力!


&emsp;

<font color="#1685a9" size="2">
下面是我的个人微信公众号，我会在里面定期更新前端的相关技术文章，欢迎大家前来讨论学习。

如果有什么问题需要老王帮忙或者想看关于某个主题的文章，也可以通过留言等方式来联系老王。
</font>


&emsp;

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200511221916465.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM5NTA2NTUx,size_16,color_FFFFFF,t_70)




