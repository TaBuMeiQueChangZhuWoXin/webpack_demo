// webpack.config.js

module.exports = {
    devServer: {
        // 提供在服务器内部执行所有其他中间件之后执行自定义中间件的能力
        after: (app => {
            console.log(app);
        }),
        // 设置白名单 允许以下开发服务器访问
        allowedHosts: {
            // 可以使用 "." 用作子域通配符 
            // 例 .host.com => www.host.com | host.com | subdomain.host.com .......
            allowedHosts: [
                'host.com',
                'subdomain.host.com',
                'subdomain2.host.com',
                'host2.com'
            ]
        },
        // 提供在服务器内部执行自定义中间件之前执行自定义中间件的能力
        before: (app => {
            app.get('/some/path', function (req, res) {
                res.json({ custom: 'response' });
            });
        }),
        // 是否启用 ZeroConf 广播
        // Bonjour 是由 apple 实现的 ZeroConf 协议产品，会在网络中自动传播它们自己的服务信息并聆听其它设备的服务信息
        bonjour: true,
        // 控制台显示消息类型
        // 取值包括 不显示消息 none  显示错误信息 error  显示警告 warning 和 默认值 info
        clientLogLevel: "info",
        // 设置是否禁用主机检查，建议设置为 false 。因为不检查主机的应用程序容易受到DNS重新绑定攻击
        disableHostCheck: false,
        // 必须在惰性模式（ lazy: true, ）下才会生效。只有请求 index.js 时 才会进行编译
        filename: "index.js",
        //  当使用 HTML5 History API 时，重定向 404 请求响应
        // { Boolean } 任意的 404 响应都会被替代为 index.html。
        // { Object } 自定义重定向页面路径
        historyApiFallback: {
            // 重定向具体规则
            rewrites: [
                { from: /^\/$/, to: '/views/landing.html' },
                { from: /^\/subpage/, to: '/views/subpage.html' },
                { from: /./, to: '/views/404.html' }
            ],
            // 当在路径中使用 "." 符号时，需要使用 disableDotRule 配置
            disableDotRule: true
        },
        // 是否启用 webpack 的 模块热替换 (Hot Module Replacement) 特性。
        hot: true,
        // 启用 Hot Module Replacement，当编译失败时，不刷新页面。
        hotOnly: true,
        // 设置索引文件的文件名
        index: 'index.htm',
        // 切换 devServer 模式，默认为内联模式 inline
        // true: 切换到 inline 模式
        // false: 切换到 iframe 模式
        inline: false,
        // SSL .pfx文件的字节流
        pfx: '/path/to/file.pfx',
        // SSL .pfx文件的密码
        pfxPassphrase: "pfxPassphrase",
        // 用于监听的Unix套接字（而不是主机）。
        socket: 'socket',
        // 设置用于从 contentBase 提供静态文件的高级选项
        staticOptions: {
            redirect: false
        }

    }
}

