// webpack.config.js

const { resolve } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: "development",
    entry: "./src/index.js",
    output: {
        path: resolve(__dirname, './dist'),
        filename: 'index.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html"
        })
    ],
    devServer: {
        // 设置主机为 ip 便于其它同局域网的开发者访问
        host:"0.0.0.0",
        // 自动打开（默认）浏览器
        open: true,
        // 指定项目运行的端口号
        port: 9527,
        // 启动 gzip 压缩 让代码体积更小 运行速度更快
        compress: true
    }
}