// 通过注释使 eslint 忽略错误可以
// eslint-disable-next-line
const fn = () => {
  console.log('index.js');
};

new Promise((resolve) => {
  setTimeout(() => {
    resolve('hello');
  }, 2000);
}).then((res) => {
  console.log(res);
});
