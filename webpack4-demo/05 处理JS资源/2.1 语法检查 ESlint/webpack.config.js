/**
 *  @filename: webpack.config.js
 *  @Author: wang
 *  @Description: webpack的配置文件,采用 CommonJS 语法 
 */


const { resolve } = require('path');

module.exports = {
    mode: "development",
    entry: {
        index: "./src/index.js"
    },
    output: {
        filename: '[name]-[chunkhash:8].js',
        path: resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                // 排查第三方库，第三方库无需检查格式
                exclude: /node_modules/,
                use: {
                    // 使用 eslint-loader 进行检查
                    loader: "eslint-loader"
                }
            }
        ]
    },
    plugins: []
}
