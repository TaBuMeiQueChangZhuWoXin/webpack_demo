
const fn = () => {
    console.log("index.js");
}

new Promise(resolve => {
    setTimeout(() => {
        resolve('hello')
    }, 2000)
}).then(res => {
    console.log(res)
})
