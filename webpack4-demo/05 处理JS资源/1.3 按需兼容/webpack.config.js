/**
 *  @filename: webpack.config.js
 *  @Author: wang
 *  @Description: webpack的配置文件,采用 CommonJS 语法 
 */


const { resolve } = require('path');

module.exports = {
    mode:"development",
    entry:{
        index:"./src/index.js"
    },
    output: {
        filename: '[name]-[chunkhash:8].js',
        path: resolve(__dirname, "dist")
    },
    module: {
        rules:[
            {test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        // 设置 babel 通过那些规定做兼容性处理
                        presets: [
                            ['@babel/preset-env',
                                {
                                    // useBuiltIns 共有三个值：
                                    // false: 不对 polyfill 做操作
                                    // entry: 根据配置的浏览器兼容版本，引入浏览器不兼容的 polyfill
                                    // usage: 根据配置的浏览器兼容版本，以及代码中使用到的语法来 决定引入那些 polyfill
                                    useBuiltIns: 'usage',
                                    // 指定使用那个版本的 corejs 进行转换
                                    corejs: {
                                        version: 3
                                    },
                                    // 配置浏览器需要兼容的版本 key 为浏览器名称， value 为浏览器的版本号
                                    targets: {
                                        // 截至到现在 chrome 现在已经更新到 86 版本了
                                        chrome: "70",
                                        ie: "9"
                                    }
                                }
                            ]
                        ]
                    }
                }
            }
        ]
    },
    plugins: []
}
