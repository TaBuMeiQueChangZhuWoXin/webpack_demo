/**
 *  @filename: webpack.config.js
 *  @Author: wang
 *  @Description: webpack的配置文件,采用 CommonJS 语法 
 */


const { resolve } = require('path');

module.exports = {
    mode: "development",
    entry: {
        index: "./src/index.js"
    },
    output: {
        filename: '[name]-[chunkhash:8].js',
        path: resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        // 设置 babel 通过那些规定做兼容性处理
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: []
}
