/**
 *  @filename: webpack.config.js
 *  @Author: wang
 *  @Description: 通过 mini-css-extract-plugin 提取样式资源到单独的文件
 */

// 与 loader 不同，使用插件需要提前引入
// 提取 css 插件
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// 压缩 css 插件
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// 去除无用 css 插件
const PurifyPlugin = require('purifycss-webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const { resolve } = require('path');

const glob = require('glob');

// process.env.NODE_ENV = 'production';

module.exports = {
    // 开发模式
    mode: "production",
    // 入口文件
    entry: {
        a: "./src/js/a.js",
        b: "./src/js/b.js"
    },
    // 打包出口
    output: {
        filename: '[name]-[chunkhash:8].js',
        path: resolve(__dirname, "dist/static/js")
    },
    // loader 配置
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [

                    // 这里我们需要使用 MiniCssExtractPlugin 取代 style-loader 
                    // style-loader : 将 css 通过 style 标签添加到页面中 
                    // MiniCssExtractPlugin.loader: 将 JS 文件中的 css 资源提取到单独的 css 文件中
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader"
                ],
            },

        ]
    },
    // 插件
    plugins: [
        // 插件都需要通过 new 的方式来调用
        new MiniCssExtractPlugin({
            // 设置生成的文件路径及文件名
            // filename: resolve(__dirname, "./css/[name]-[chunkhash:8].css")
            filename: "../css/[name]-[chunkhash:8].css"
        }),
        // 直接使用默认配置即可
        new OptimizeCssAssetsPlugin(),

        new HtmlWebpackPlugin({
            // 复制 './src/a.html' 文件，并自动引入打包输出的所有资源（JS/CSS）
            template: './src/a.html',
            chunks: ['a'],
            filename: resolve(__dirname, "./dist/a.html")
        }),
        new HtmlWebpackPlugin({
            template: './src/b.html',
            chunks: ['b'],
            filename: resolve(__dirname, "./dist/b.html")
        }),
        // 
        new PurifyPlugin({
            paths: glob.sync(resolve(__dirname, 'src/*.html'))
        }),
        require('postcss-preset-env')()
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                //打包公共模块
                commons: {
                    // initial 表示提取每个入口文件中的公共部分
                    chunks: 'initial',
                    // 设置公共部分提取的最少文件数为 2
                    minChunks: 2,
                    // 设置提取公共部分最小的大小
                    // 假设值为 1* 1024 则表示 1KB 以下的文件不参与提取
                    minSize: 0,
                    // 提取出来的文件命名
                    name: 'common'
                }
            }
        }
    }
}