/**
 *  @filename: webpack.config.js
 *  @Author: wang
 *  @Description: webpack的配置文件,采用 CommonJS 语法 
 */

// 引入 NodeJS 中的 Path 模块暴露的 resolve 方法来处理路径问题 
// 同样可以使用 join 方法来拼接路径
const { resolve } = require('path');

// CommonJS 语法暴露配置文件
module.exports = {
    // 开发模式
    mode: "development",
    // 入口文件
    entry: {
        index: "./src/index.js"
    },
    // 打包出口
    output: {
        // 设置打包后的文件名为 入口名 - chunkhash 前八位 .js
        filename: '[name]-[chunkhash:8].js',
        // 设置打包后的文件位于 dist 文件夹
        // __dirname 为 NodeJs 中的变量，代表当前文件的绝对目录
        path: resolve(__dirname, "dist")
    },
    // loader 配置
    module: {
        rules: [
            // 详细配置
            {
                // 使用正则匹配所有 .css 结尾的文件
                test: /\.css$/,
                // 通过 style-loader 和 css-loader 处理匹配到的文件
                // loader 的执行顺序是从下到上的 千万注意 loader 的书写顺序
                use: [
                    // 通过 style 标签将 JS 文件中的 css 资源添加到页面的 head 中
                    "style-loader",
                    // 将 css 文件变成 CommonJS 的模块资源，加载到 JS 文件中
                    "css-loader"
                ],
            },
            {
                test: /\.less$/,
                // less-loader: 将 .less 文件编译成 .css 文件
                use: ["style-loader", "css-loader", "less-loader"]
            },
            {
                test: /\.scss$/,
                // sass-loader: 将 .sass 文件编译成 .css 文件
                use: ["style-loader", "css-loader", "sass-loader"]
            },

        ]
    },
    // 插件
    plugins: []
}
