
const { resolve } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: "development",
    entry: {
        // 将单入口改为多入口
        list: "./src/list.html",
        detail: "./src/detail.html"
    },
    output: {
        // 输出文件名改为 chunkname _ chunkhash
        filename: "[name]-[chunkhash:8].js",
        path: resolve(__dirname, "dist/"),
    },
    module: {

    },
    plugins: [
        // 打包 list.html
        new HtmlWebpackPlugin({
            template: "./src/list.html",
            filename: "list.html",
            chunks: ["list"]
        }),
        // 打包 detail.html
        new HtmlWebpackPlugin({
            template: "./src/detail.html",
            filename: "detail.html",
            chunks: ["detail"]
        }),
    ],
};
