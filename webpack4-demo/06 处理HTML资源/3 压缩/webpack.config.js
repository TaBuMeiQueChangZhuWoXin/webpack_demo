
const { resolve } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: "development",
    entry: "./src/js/index.js",
    output: {
        filename: "js/index.js",
        path: resolve(__dirname, "dist/"),
    },
    module: {
       
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html",
            // 设置压缩方式
            minify: {
                // 删除空行缩进等
                collapseWhitespace: true,
            }
        })
    ]
};