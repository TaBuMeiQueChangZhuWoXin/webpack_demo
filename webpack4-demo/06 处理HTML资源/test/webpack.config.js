
const { resolve } = require('path');

module.exports = {
    mode: "production",
    entry: "./src/index.js",
    output: {
        filename: "index.js",
        path: resolve(__dirname, "dist/"),
    },
    module: {
       
    },
    plugins: [
    
    ]
};