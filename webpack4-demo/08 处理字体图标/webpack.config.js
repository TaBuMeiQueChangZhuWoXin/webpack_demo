
const { join } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: "development",
    entry: "./src/index.js",
    output: {
        filename: "index.js",
        path: join(__dirname, '/dist/')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                // 排除 html css js 文件
                exclude: /\.(css|js|html)/,
                loader: "file-loader",
                options: {
                    name: "[name]-[hash:5].min.[ext]",
                    outputPath: 'fonts/'
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html'
        })
    ]
}